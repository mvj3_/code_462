require 'net/http'

Net::HTTP.start('www.eoe.cn') do |http|
  http.open_timeout = 2
  http.read_timeout = 2
  http.head('/').each { |k, v| puts "#{k}: #{v}" }
end